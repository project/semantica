<?php

namespace Drupal\semantica\Plugin;

use Drupal\layout_plugin\Plugin\Layout\LayoutBase;
use Drupal\Core\Form\FormStateInterface;


class SemanticaBaseLayout extends LayoutBase {

  private $breakpointManager;

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'semantica' => [],
      'semantica_outer_wrapper' => 'segment',
      'semantica_outer_style' => 'basic',
      'semantica_wrapper' => 0,
      'semantica_variation' => []
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected static function getContainerStyles() {
    return [
      'basic' => t('Basic'),
      'raised' => t('Raised'),
      'stacked' => t('Stacked'),
      'pilled' => t('Pilled'),
      'vertical' => t('Vertical'),
      'text' => t('Text')
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected static function getWrapperOptions() {
    return [
      0 => t('None'),
      'div' => 'Div',
      'span' => 'Span',
      'section' => 'Section',
      'article' => 'Article',
      'header' => 'Header',
      'footer' => 'Footer',
      'aside' => 'Aside',
      'figure' => 'Figure',
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected static function getGridVariations() {
    return [
      0 => t('None'),
      'stackable' => t('Stackable'),
      'doubling' => t('Doubling'),
      'vertically' => t('Vertically Divided (needs divided)'),
      'divided' => t('Divided'),
      'internally' => t('Internally celled (needs celled)'),
      'celled' => t('Celled'),
      'padded' => t('Padded'),
    ];
  }

  private function buildConfigArray($config) {
    $iterator = new \RecursiveIteratorIterator(new \RecursiveArrayIterator($config),\RecursiveIteratorIterator::SELF_FIRST);

    $config_string = [];
    $config_semantica = [] ;
    $current_depth = 0 ;
    $remove = 0 ;
    foreach($iterator as $key=>$value) {
      if ( $iterator->getDepth() == 0 ) {
        $config_string = [];
      }
      $config_string[] = $key;
      if ( !is_array($value) ) {
        $config_semantica[implode('_',$config_string)] = $value;
        if ( $remove == 0 ) $remove = 1 ;
        for ($i = 0; $i < $remove; $i++) {
          array_pop($config_string);
        }
        $remove = 0;
      } else {
        $remove++;
      }
    }
    return $config_semantica;
  }

  /**
   * {@inheritdoc}
   */
  public function build(array $regions) {
    $build = parent::build($regions);
    $regs = $this->getRegionDefinitions();
    $config = $this->getConfiguration();
    $build['#settings'] = array_merge($build['#settings'],$this->buildConfigArray($build['#settings']['semantica']));

    foreach($regs as $r => $definition) {
      $build['#settings'][$r . '_css_attributes'] = '';
      $default = $definition['grid'];
      if ( isset($build['#settings']['semantica'][$r]) ) {
        $breaks = array_reverse($build['#settings']['semantica'][$r]['breakpoints']);
        foreach($breaks as $break => $settings) {
          if (!empty($settings['enable'])) {
            $build['#settings'][$r . '_css_attributes'] .= $settings['grid'] . ' wide ' . $break . ' ';
          }
        }
      }
      if ( !empty($build['#settings'][$r . '_css_attributes']) ) {
        $build['#settings'][$r . '_css_attributes'] .= 'column';
      } else {
        $build['#settings'][$r . '_css_attributes'] .= $default . ' wide column';
      }
    }
    if ($build['#settings']['semantica_outer_style'] == '0') {
      $build['#settings']['semantica_outer_style'] = '';
    }
    $build['#settings']['semantica_outer_style'] = implode(' ', array_keys($build['#settings']['semantica_variation'] )) . ' ' . $build['#settings']['semantica_outer_style'];

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  protected static function getContainerTypes() {
    return [
      0 => t('None'),
      'segment' => t('UI Segment'),
      'container' => t('UI Container')
    ];
  }


  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->breakpointManager = \Drupal::service('breakpoint.manager');
  }

  protected function getBreakpoints() {
    $config = \Drupal::config('system.theme');
    return $this->breakpointManager->getBreakpointsByGroup($config->get('default'));
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $configuration = $this->getConfiguration();
    $regions = $this->getRegionDefinitions();

    // Add wrappers.
    $styles = SemanticaBaseLayout::getContainerStyles();
    $container = SemanticaBaseLayout::getContainerTypes();
    $wrapper = SemanticaBaseLayout::getWrapperOptions();

    $form['semantica_wrapper'] = [
      '#type' => 'select',
      '#options' => $wrapper,
      '#title' => t('Default container for wrapper'),
      '#default_value' => $configuration['semantica_wrapper'] ? $configuration['semantica_wrapper'] : FALSE
    ];

    $form['semantica_outer_wrapper'] = [
      '#type' => 'select',
      '#options' => $container,
      '#title' => t('Default container for wrapper'),
      '#default_value' => $configuration['semantica_outer_wrapper'] ? $configuration['semantica_outer_wrapper'] : 'segment',
      '#states' => array(
        'visible' => array(
          ':input[name="layout_configuration[semantica_wrapper]"]' => array("!value" => 0),
        ),
      ),
    ];

    $form['semantica_outer_style'] = [
      '#type' => 'select',
      '#options' => $styles,
      '#title' => t('Default styles for wrapper'),
      '#default_value' => $configuration['semantica_outer_style'] ? $configuration['semantica_outer_style'] : 'basic',
      '#states' => array(
        'visible' => array(
          ':input[name="layout_configuration[semantica_outer_wrapper]"]' => array("!value" => 0),
          ':input[name="layout_configuration[semantica_wrapper]"]' => array("!value" => 0),
        ),
      ),
    ];

    $form['semantica_variation'] = array(
      '#type' => 'checkboxes',
      '#multiple' => TRUE,
      '#options' => $this->getGridVariations(),
      '#title' => t('Variations for grid'),
      '#default_value' => !empty($configuration['semantica_variation']) ? $configuration['semantica_variation'] : 'div',
    );

    if ( $this->isGrid() ) {
      $form['semantica'] = [
        '#type' => 'container'
      ];

      foreach ($regions as $region_name => $region_definition) {
        $form['semantica'][$region_name] = [
          '#type' => 'fieldset',
          '#title' => t('Settings for @region', array('@region' => $region_definition['label'])),
        ];
        $form['semantica'][$region_name]['breakpoints'] = [
          '#type' => 'fieldset',
          '#title' => t('Specify breakpoint handling'),
          '#tree' => TRUE
        ];
        $this->buildGridForm($form['semantica'][$region_name]['breakpoints'], $region_name);
      }

    }

    return $form;
  }

  function buildGridForm(&$form, $region_name) {
    $breakpoints = $this->getBreakpoints();
    $configuration = $this->getConfiguration();
    $conf = @$configuration['semantica'][$region_name]['breakpoints'];
    foreach ($breakpoints as $plugin_id => $break) {
      $plugin_id = explode('.', $plugin_id);
      $plugin_id = array_pop($plugin_id);

      if (!isset($conf[$plugin_id])) {
        $conf[$plugin_id] = [
          'enable' => 0
        ];
      }

      $state_trigger = ':input[name="layout_configuration[semantica]['.$region_name.'][breakpoints]['.$plugin_id.'][enable]"]';
      $form[$plugin_id] = [
        '#type' => 'container',
        '#tree' => TRUE
      ];
      $form[$plugin_id]['enable'] = [
        '#type' => 'checkbox',
        '#title' => t('Enable configuration for breakpoint @break', ['@break' => $break->getLabel() ]),
        '#default_value' => !empty($conf[$plugin_id]['enable']) ? $conf[$plugin_id]['enable'] : 0,
        '#return_value' => 1
      ];
      $form[$plugin_id]['grid'] = [
        '#type' => 'select',
        '#title' => t('Set definition for grid @break', ['@break' => $break->getLabel() ]),
        '#options' => $this->getGridOptions(),
        '#default_value' => !empty($conf[$plugin_id]['grid']) ? $conf[$plugin_id]['grid'] : 0,
        '#states' => [
          'visible' => [
            $state_trigger => array("checked" => TRUE),
          ],
        ],
      ];
    }
  }

  protected function getGridOptions() {
    return [
      'one' => 1,
      'two' => 2,
      'three' => 3,
      'four' => 4,
      'five' => 5,
      'six' => 6,
      'seven' => 7,
      'eight' => 8,
      'nine' => 9,
      'ten' => 10,
      'eleven' => 11,
      'twelve' => 12,
      'thirteen' => 13,
      'fourteen' => 14,
      'fiveteen' => 15,
      'sixteen'  => 16,
      0 => t('Do not display')
    ];
  }

  /**
   * {@inheritdoc}
   */
  function isGrid() {
    return (!isset($this->pluginDefinition['grid'])) ? FALSE : (boolean) $this->pluginDefinition['grid'];
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->submitConfigurationFormGrid($form, $form_state);
    $defaults = $this->defaultConfiguration();
    $this->configuration['semantica_variation'] = array_filter($form_state->getValue('semantica_variation'));

    $this->configuration['semantica_wrapper'] = $form_state->getValue('semantica_wrapper', $defaults['semantica_wrapper']);
    if ( $this->configuration['semantica_wrapper'] == 0 ) {
      $this->configuration['semantica_outer_wrapper'] = 0;
      $this->configuration['semantica_outer_style'] = 0 ;
      return ;
    }

    $this->configuration['semantica_outer_wrapper'] = $form_state->getValue('semantica_outer_wrapper', $defaults['semantica_outer_wrapper']);
    if ( $this->configuration['semantica_outer_wrapper'] == 0 ) {
      $this->configuration['semantica_outer_style'] = 0 ;
      return ;
    }
    $this->configuration['semantica_outer_style'] = $form_state->getValue('semantica_outer_style', $defaults['semantica_outer_style']);
  }

  /**
  * {@inheritdoc}
  */
  public function submitConfigurationFormGrid(array &$form, FormStateInterface $form_state) {

  }

}
