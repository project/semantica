# Core Partials

A collection of partials that do not get compiled directly that are used
as assets for Sass.

A general rule-of-thumb is if a Sass partials' contents do not actually
print something into the compiled CSS, it belongs here.

## Partial files

Filename          | Purpose
----------------- | ---------------------------------------------
`_functions.scss` | Pulls together Sass functions used across the project.
`_variables.scss` | Pulls together Sass variables used across the project.
`_mixins.scss`    | Pull together Sass mixins used across the project.
`_vendors.scss`   | Wiredep pulls in vendor tools like Grid frameworks here: no need to edit manually.

Each of these partial files has a corresponding directory. Each function
or mixin is defined within its own partial, and different types of
variables are defined in their own partials as well. This helps to more
easily find what you are looking for, rather than searching through huge
files. Defining items in individual files makes it easier to share
items of general use with other projects.

Each individual function, variable type and mixin partial must be
manually imported in the corresponding core partial (e.g. `_functions.scss`),
since one function could depend on another function, for example.
