# Mixin Partials

A collection of partials that define mixins used across the project.

## Mixins

Filename                      | Purpose
----------------------------- | ----------------------------------------
`_clearfix.scss`              | Clears floats for any elements nested inside this element.
`_element-focusable.scss`     | Makes an element visually hidden by default, but visible when focused.
`_element-invisible.scss`     | Makes an element visually hidden, but accessible.
`_element-invisible-off.scss` | Turns off the element-invisible effect.
`_respond-to.scss`            | Connects a Sass Breakpoint key with a Susy grid.

As you add new mixins, add a short definition for each to this file.
