# Variable Partials

A collection of partials that define variables used across the project.

## Variable groups

Filename              | Purpose
--------------------- | ---------------------------------------------
`_breakpoints.scss`   | Media query values.
`_colors.scss`        | Colors and their variations.
`_font-stacks.scss`   | Groups of fonts to provide sensible fallbacks.
`_miscellaneous.scss` | Other variables without obvious groupings.
`_typography.scss`    | Line heights and other typograhical variables.

As you add new variable groups, add a short definition for each to this
file.
