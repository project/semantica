# Function Partials

A collection of partials that define functions used across the project.

## Functions

Filename                    | Purpose
--------------------------- | ------------------------------------------
`_map-deep-get.scss`        | Fetches nested maps.
`_map-has-nested-keys.scss` | Checks if a map has nested keys inside it.

As you add new functions, add a short definition for each to this file.
