module.exports = function (gulp, plugins) {
    return function () { 
    	return gulp.src('./js /**/*.js')
  	  		.pipe(plugins.jshint())
  	  		.pipe(plugins.jshint.reporter("jshint-stylish"));
  };
}