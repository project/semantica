module.exports = function (gulp, plugins , opts ) {
    return function () {
    		  files = opts.gulp.js.paths ;
    		  files = files.concat(plugins.mainBowerFiles('**/*.js',{debugging:true}));
	    	  return gulp.src(files)
	    	    .pipe(plugins.concat('scripts.js'))
	    	    .pipe(gulp.dest('vendor/js'));
	    };
}
