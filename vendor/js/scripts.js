/**
 * @file
 * Some small enhancements for semantica theme.
 */

(function ($, Drupal) {
  'use strict';
  Drupal.behaviors.semantica_menuVisibility = {
    attach: function (context, settings) {
      $('body',context).once('semantica-visibility-check').each(function(){
        var $body = $(this);
        var $toolbar = $body.find('#toolbar-bar');
        var $menu = $body.find('.main-menu-top-container div.main-menu');
        $body.find('.main-menu-top-container')
          .visibility({
            continuous : true,
            once: false,
            initialCheck: true,
            onTopPassed: function() {
              $menu.addClass('top-fixed').addClass('fluid');
              if ($toolbar) {
                var padding = 0;
                if ($body.hasClass('toolbar-horizontal')) {
                   padding = $toolbar.find('.toolbar-menu').height();
                }
                $menu.css({top: $toolbar.height() + padding })
              }
            },
            onOnScreen: function() {
              $menu.removeClass('top-fixed').removeClass('fluid');
            }
          })
      })
    }
  };
})(jQuery, Drupal);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJpbml0LmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQGZpbGVcbiAqIFNvbWUgc21hbGwgZW5oYW5jZW1lbnRzIGZvciBzZW1hbnRpY2EgdGhlbWUuXG4gKi9cblxuKGZ1bmN0aW9uICgkLCBEcnVwYWwpIHtcbiAgJ3VzZSBzdHJpY3QnO1xuICBEcnVwYWwuYmVoYXZpb3JzLnNlbWFudGljYV9tZW51VmlzaWJpbGl0eSA9IHtcbiAgICBhdHRhY2g6IGZ1bmN0aW9uIChjb250ZXh0LCBzZXR0aW5ncykge1xuICAgICAgJCgnYm9keScsY29udGV4dCkub25jZSgnc2VtYW50aWNhLXZpc2liaWxpdHktY2hlY2snKS5lYWNoKGZ1bmN0aW9uKCl7XG4gICAgICAgIHZhciAkYm9keSA9ICQodGhpcyk7XG4gICAgICAgIHZhciAkdG9vbGJhciA9ICRib2R5LmZpbmQoJyN0b29sYmFyLWJhcicpO1xuICAgICAgICB2YXIgJG1lbnUgPSAkYm9keS5maW5kKCcubWFpbi1tZW51LXRvcC1jb250YWluZXIgZGl2Lm1haW4tbWVudScpO1xuICAgICAgICAkYm9keS5maW5kKCcubWFpbi1tZW51LXRvcC1jb250YWluZXInKVxuICAgICAgICAgIC52aXNpYmlsaXR5KHtcbiAgICAgICAgICAgIGNvbnRpbnVvdXMgOiB0cnVlLFxuICAgICAgICAgICAgb25jZTogZmFsc2UsXG4gICAgICAgICAgICBpbml0aWFsQ2hlY2s6IHRydWUsXG4gICAgICAgICAgICBvblRvcFBhc3NlZDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICRtZW51LmFkZENsYXNzKCd0b3AtZml4ZWQnKS5hZGRDbGFzcygnZmx1aWQnKTtcbiAgICAgICAgICAgICAgaWYgKCR0b29sYmFyKSB7XG4gICAgICAgICAgICAgICAgdmFyIHBhZGRpbmcgPSAwO1xuICAgICAgICAgICAgICAgIGlmICgkYm9keS5oYXNDbGFzcygndG9vbGJhci1ob3Jpem9udGFsJykpIHtcbiAgICAgICAgICAgICAgICAgICBwYWRkaW5nID0gJHRvb2xiYXIuZmluZCgnLnRvb2xiYXItbWVudScpLmhlaWdodCgpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAkbWVudS5jc3Moe3RvcDogJHRvb2xiYXIuaGVpZ2h0KCkgKyBwYWRkaW5nIH0pXG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBvbk9uU2NyZWVuOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgJG1lbnUucmVtb3ZlQ2xhc3MoJ3RvcC1maXhlZCcpLnJlbW92ZUNsYXNzKCdmbHVpZCcpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH0pXG4gICAgICB9KVxuICAgIH1cbiAgfTtcbn0pKGpRdWVyeSwgRHJ1cGFsKTtcbiJdLCJmaWxlIjoiaW5pdC5qcyIsInNvdXJjZVJvb3QiOiIvc291cmNlLyJ9
