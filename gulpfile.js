/**
 * @file
 */

var opts = {pattern: ['gulp-*', 'gulp.*','main-bower-files']};

var gulp = require('gulp');
var fs = require('fs');
var plugins = require('gulp-load-plugins')(opts);
var del = require('del');
var path = require('path');
var runSequence = require('run-sequence');
var paths = ['styles'];

var config = {
  gulp: {
    modernizr : {},
    js: {
      paths: ['js/**/*.js']
    },
    css: {
      paths: []
    },
    sass: {
      paths: [],
      node : [],
      includePaths : []
    }
  }
};

plugins.mainBowerFiles('**/*.scss').forEach(function(v){
  paths = paths.concat(path.dirname(v));
})

plugins.mainBowerFiles('**/*.sass').forEach(function(v){
  paths = paths.concat(path.dirname(v));
})

config.sass = {
  includePaths : paths
};

const semantic_build = require('./semantic/tasks/build.js');
const semantic_clean = require('./semantic/tasks/clean.js');
const semantic_watch = require('./semantic/tasks/watch.js');


gulp.task('sass', require('./tasks/sass')(gulp, plugins,config));
gulp.task('jshint', require('./tasks/jshint')(gulp, plugins,config));
gulp.task('slint', ['concat'], require('./tasks/scss-lint')(gulp, plugins,config));
gulp.task('concat', require('./tasks/concat')(gulp, plugins,config));
gulp.task('concat_css', require('./tasks/concat_css')(gulp, plugins,config));
gulp.task('semanticui-build', semantic_build);

gulp.task('semanticui-watch', semantic_watch);

gulp.task('clean', function (cb) {
  del(['vendor/**/*']);
});

gulp.task('watch',function() {
  gulp.watch(['scss/**/*.scss'], ['sass']);
  gulp.watch(['js/**/*.js'], ['jshint','concat']);
  gulp.watch(['bower.json'], ['concat_css','concat']);
})

gulp.task('semantica-watch',function(cb){
  runSequence('semanticui-watch',cb);
});

gulp.task('build',function(){
  runSequence('semanticui-build','sass','bundle')
});

gulp.task('bundle',['concat','concat_css'],function(cb){
});

gulp.task('default', function(cb) {
  runSequence('build',['semantica-watch', 'watch'],cb);
})

gulp.task('clean',function() {
  del('vendor/**/*');
  del('semantic/dist/**/*');
})

gulp.task('rebuild',['semantic-clean','clean','semantic-build','sass','concat_css','concat'],function() {

});
