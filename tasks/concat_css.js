/**
 * @file
 * Task for bundle css files.
 */

module.exports = function (gulp, plugins,opts) {
  return function () {
    files = opts.gulp.css.paths;
    files = files.concat(plugins.mainBowerFiles('**/*.css',{debugging:true}));
    return gulp.src(files)
      .pipe(plugins.concat("bundles.css"))
      .pipe(gulp.dest('vendor/css'));
  };
};
