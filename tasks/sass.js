/**
 * @file
 * Task for bundle js files.
 */

module.exports = function (gulp, plugins,opts) {
  return function () {
    gulp.src('scss/styles.scss')
      .pipe(plugins.sourcemaps.init())
      .pipe(plugins.plumber())
      .pipe(plugins.sass({
        includePaths: opts.sass.includePaths,
        sourceComments : true
      }))
      .pipe(plugins.sourcemaps.write())
      .pipe(gulp.dest('vendor/css'));
  };
}
