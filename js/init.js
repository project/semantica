/**
 * @file
 * Some small enhancements for semantica theme.
 */

(function ($, Drupal) {
  'use strict';
  Drupal.behaviors.semantica_menuVisibility = {
    attach: function (context, settings) {
      $('body',context).once('semantica-visibility-check').each(function(){
        var $body = $(this);
        var $toolbar = $body.find('#toolbar-bar');
        var $menu = $body.find('.main-menu-top-container div.main-menu');
        $body.find('.main-menu-top-container')
          .visibility({
            continuous : true,
            once: false,
            initialCheck: true,
            onTopPassed: function() {
              $menu.addClass('top-fixed').addClass('fluid');
              if ($toolbar) {
                var padding = 0;
                if ($body.hasClass('toolbar-horizontal')) {
                   padding = $toolbar.find('.toolbar-menu').height();
                }
                $menu.css({top: $toolbar.height() + padding })
              }
            },
            onOnScreen: function() {
              $menu.removeClass('top-fixed').removeClass('fluid');
            }
          })
      })
    }
  };
})(jQuery, Drupal);
